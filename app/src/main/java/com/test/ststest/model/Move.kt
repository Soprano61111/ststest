package com.test.ststest.model

class Move {
    var fromPlace: String? = null
    var toPlace: String? = null
    var estimateTime: TimeInterval? = null

    enum class TimeInterval {
        SECOND, MINUTE, HOUR, DAY, WEEK, MONTH, YEAR
    }
}