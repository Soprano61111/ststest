package com.test.ststest.model

import java.util.*

class Event {
    var startTime: Date? = null
    var endTime: Date? = null
    var name: String? = null
}