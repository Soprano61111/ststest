package com.test.ststest.model

import java.util.*

data class Notice(
    val flightDate: Date? = null,
    val gate: String? = null
)