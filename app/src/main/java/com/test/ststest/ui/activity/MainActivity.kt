package com.test.ststest.ui.activity

import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.test.ststest.R
import com.test.ststest.presentation.presenter.TestPresenter
import com.test.ststest.presentation.view.TestView

@InjectViewState
class MainActivity : MvpActivity() , TestView {

    @InjectPresenter
    lateinit var mTestPresenter: TestPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //todo через презентер запрашиваем список случайно длины от 10 до 100 элементов


    }

    //todo тут функция viewState кидает данные в адаптер и русует их
}
