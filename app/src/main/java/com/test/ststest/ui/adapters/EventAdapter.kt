package com.test.ststest.ui.adapters

class EventAdapter : KDelegateAdapter<TextViewModel>() {

    override fun onBind(item: TextViewModel, viewHolder: KViewHolder) =
        with(viewHolder) {
            tv_title.text = item.title
            tv_description.text = item.description
        }

    override fun isForViewType(items: List<*>, position: Int) =
        items[position] is TextViewModel

    override fun getLayoutId(): Int = R.layout.text_item
}